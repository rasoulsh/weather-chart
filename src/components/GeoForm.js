import React, {
  useState,
  useEffect,
  useCallback,
} from "react";
import Geocode from "react-geocode";

Geocode.setApiKey(
  "AIzaSyDp4os33WF6-4d-xFVyL0HsUUHN7dOml_w"
);

const GeoForm = ({ setLatLng }) => {
  const [value, setValue] = useState("Gorgan");

  const getLatLng = useCallback(
    (address) => {
      Geocode.fromAddress(address).then((res) => {
        const {
          lat,
          lng,
        } = res.results[0].geometry.location;
        setLatLng({ lat, lng });
      });
    },
    [setLatLng]
  );

  useEffect(() => {
    getLatLng(value);
  }, []);

  const handleSubmit = (e) => {
    e.preventDefault();
    getLatLng(value);
  };

  return (
    <form onSubmit={handleSubmit}>
      <input
        type="text"
        value={value}
        onChange={(e) => setValue(e.target.value)}
      />
    </form>
  );
};

export default GeoForm;
